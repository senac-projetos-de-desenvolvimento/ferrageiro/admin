# Sistema WEB

### Repositório do Ferrageiro sistema de vendas online
Ferragem Luz - Vendas Online 

## Configurações do Projeto

O sistema funciona em conjunto com a [API](https://gitlab.com/projeto-de-desenvolvimento-2020.2/ferrageiro/api).

## Requisitos para instalar o projeto

- **Axios** - npm install axios
- **Vuesax** - npm install vuesax@next  

## Tecnologias

[VueJS](https://vuejs.org/)

[Javascript](https://www.javascript.com/)

## Clone do Projeto
Basta clicar [aqui](https://gitlab.com/projeto-de-desenvolvimento-2020.2/ferrageiro/admin.git) e logo clicar em clone. 

## Executar projeto

1. Inicie o MySQL e APACHE (XAMPP);
2. Execute o app.py na pasta da API;
3. Abra a pasta do projeto pelo GitBash, Prompt de Comando (ou outro de preferência) e execute o projeto com **npm run serve**
4. Abra o navegador com a URL que irá aparecer no GitBash e o sistema irá iniciar.
5. Para logar no sistema, deve ser feito um login através de um "post" pelo Insomnia, Postmann ou outro de sua preferencia. Para realizar o cadastro do primeiro perfil basta enviar um json com nome, email e senha para: http://localhost:5000/usuarios 
Exemplo: {
	"nome": "Roberto Pinho",
	"email": "administrador@ferrageiro.com",
	"senha": "SenhaAdmin123"
}
