-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 16-Jul-2021 às 21:41
-- Versão do servidor: 10.4.18-MariaDB
-- versão do PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `ferragem`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `nome`) VALUES
(1, 'Ferragens'),
(2, 'Ferramentas'),
(3, 'Pintura');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cidade`
--

CREATE TABLE `cidade` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `uf_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `cidade`
--

INSERT INTO `cidade` (`id`, `nome`, `uf_id`) VALUES
(1, 'Pelotas', 1),
(2, 'Rio Grande', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `email` varchar(120) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `telefone` varchar(11) DEFAULT NULL,
  `endereco_logradouro` varchar(100) DEFAULT NULL,
  `endereco_bairro` varchar(40) DEFAULT NULL,
  `cidade_id` int(11) NOT NULL,
  `endereco_numero` int(11) DEFAULT NULL,
  `endereco_complemento` varchar(60) DEFAULT NULL,
  `endereco_tipo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `nome`, `cpf`, `email`, `senha`, `telefone`, `endereco_logradouro`, `endereco_bairro`, `cidade_id`, `endereco_numero`, `endereco_complemento`, `endereco_tipo`) VALUES
(6, 'Roberto Pinho Cardozo', '02188644000', 'roberto@gmail.com', '808b54f0dc1082c417c17a016899c930', '53981066026', 'E - IL Centenário, sala 1603', 'Fragata', 1, 255, NULL, NULL),
(7, 'Laura Soares', '036850515', 'laura@gmail.com', '808b54f0dc1082c417c17a016899c930', '53981066026', 'Av asdasd', 'Cassino', 1, 1545, 'casa', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedores`
--

CREATE TABLE `fornecedores` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `cnpj` varchar(14) NOT NULL,
  `telefone` varchar(15) NOT NULL,
  `email` varchar(60) NOT NULL,
  `representante` varchar(30) NOT NULL,
  `telefone_representante` varchar(15) NOT NULL,
  `email_representante` varchar(60) NOT NULL,
  `cidade_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `fornecedores`
--

INSERT INTO `fornecedores` (`id`, `nome`, `cnpj`, `telefone`, `email`, `representante`, `telefone_representante`, `email_representante`, `cidade_id`) VALUES
(1, 'Gb', '11165651651651', '53981167169', 'email@email.com', 'Roberto', '53981167169', 'roberto@email.com', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `marcas`
--

CREATE TABLE `marcas` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `marcas`
--

INSERT INTO `marcas` (`id`, `nome`) VALUES
(1, 'Diversas'),
(2, 'Worker'),
(3, 'DTOOLS'),
(4, 'MTX'),
(5, 'TEKBONDER');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido`
--

CREATE TABLE `pedido` (
  `id` int(11) NOT NULL,
  `data_compra` datetime NOT NULL,
  `data_entrega` datetime NOT NULL,
  `desconto` float DEFAULT NULL,
  `valor_pedido` float DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `observacao` varchar(200) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `pedido`
--

INSERT INTO `pedido` (`id`, `data_compra`, `data_entrega`, `desconto`, `valor_pedido`, `status`, `observacao`, `id_cliente`, `id_usuario`) VALUES
(1, '2021-06-27 23:50:57', '2021-06-27 23:50:57', NULL, 7.62, 'fechado', NULL, 6, NULL),
(2, '2021-07-03 13:08:51', '2021-07-03 13:08:51', NULL, 13.6, 'fechado', NULL, NULL, NULL),
(3, '2021-07-03 20:27:07', '2021-07-03 20:27:07', NULL, 0.09, 'entregue', NULL, 6, NULL),
(4, '2021-07-03 20:30:16', '2021-07-03 20:30:16', NULL, 5.31, 'fechado', NULL, 6, NULL),
(5, '2021-07-03 21:08:59', '2021-07-03 21:08:59', NULL, 988.47, 'entregue', NULL, 6, NULL),
(6, '2021-07-07 19:20:57', '2021-07-07 19:20:57', NULL, 1.62, 'fechado', NULL, NULL, NULL),
(7, '2021-07-14 19:27:58', '2021-07-14 19:27:58', NULL, 1466.6, 'aberto', NULL, NULL, NULL),
(8, '2021-07-14 20:35:12', '2021-07-14 20:35:12', NULL, 2.65, 'fechado', NULL, NULL, NULL),
(9, '2021-07-14 20:35:59', '2021-07-14 20:35:59', NULL, 1177.96, 'aberto', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `data_compra` datetime NOT NULL,
  `nome` varchar(200) NOT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  `imagem` varchar(300) DEFAULT NULL,
  `unidade` varchar(2) NOT NULL,
  `quant` int(11) NOT NULL,
  `quant_minima` int(11) NOT NULL,
  `valor_compra` float NOT NULL,
  `porcentagem` int(11) DEFAULT NULL,
  `valor_venda` float NOT NULL,
  `peso` float DEFAULT NULL,
  `destaque` varchar(1) DEFAULT NULL,
  `marca_id` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `fornecedor_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `data_compra`, `nome`, `descricao`, `imagem`, `unidade`, `quant`, `quant_minima`, `valor_compra`, `porcentagem`, `valor_venda`, `peso`, `destaque`, `marca_id`, `categoria_id`, `fornecedor_id`) VALUES
(1, '2021-06-28 00:36:48', 'Bucha 5', NULL, 'http://localhost:8080/img/produtos/bucha-s.jpg', 'UN', 4970, 300, 0.04, NULL, 0.09, NULL, '', 1, 1, 1),
(2, '2021-06-28 00:36:48', 'Bucha 6', '', NULL, 'UN', 4967, 300, 0.05, NULL, 0.09, NULL, '', 1, 1, 1),
(3, '2021-06-28 00:36:48', 'Bucha 8', '', NULL, 'UN', 4892, 300, 0.06, NULL, 0.1, NULL, '', 1, 1, 1),
(4, '2021-06-28 00:36:48', 'Bucha 10', '', NULL, 'UN', 4993, 300, 0.08, NULL, 0.11, NULL, '', 1, 1, 1),
(5, '2021-06-28 00:36:48', 'Bucha 12', '', NULL, 'UN', 5000, 300, 0.09, NULL, 0.12, NULL, '', 1, 1, 1),
(6, '2021-07-08 02:34:58', 'Serra Tico Tico Elétrica Worker 800W', NULL, 'http://localhost:8080/img/produtos/ticotico-worker.jpg', 'UN', 11, 3, 320, 40, 448, NULL, '⭐', 2, 2, 1),
(7, '2021-07-08 02:41:02', 'Jogo Chave Ferramentas Maleta Soquete Cromo 110 peças Worker', NULL, 'http://localhost:8080/img/produtos/jogo110worker.jpg', 'UN', 29, 5, 299, 40, 418.6, NULL, '⭐', 2, 2, 1),
(8, '2021-07-08 02:45:04', 'Jogo de Chaves Combinadas 6 a 22 mm 12 peças Worker', NULL, 'http://localhost:8080/img/produtos/chaves-combinadas-6-a-22-workerjpg.jpg', 'UN', 21, 5, 40, 50, 59.99, NULL, '⭐', 2, 2, 1),
(9, '2021-07-14 22:25:37', 'Martelo Unha Cabeça 27 Mm Com Cabo De Fibra Mtx', NULL, 'http://:8080/img/produtos/martelo27mm.jpg', 'UN', 44, 5, 17, 40, 23.8, NULL, '⭐', 4, 2, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto_pedido`
--

CREATE TABLE `produto_pedido` (
  `id` int(11) NOT NULL,
  `produto_id` int(11) DEFAULT NULL,
  `pedido_id` int(11) DEFAULT NULL,
  `quant_compra` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `produto_pedido`
--

INSERT INTO `produto_pedido` (`id`, `produto_id`, `pedido_id`, `quant_compra`) VALUES
(1, 5, 1, 7),
(2, 2, 1, 12),
(3, 1, 1, 50),
(4, 4, 1, 10),
(5, 1, 2, 20),
(6, 3, 2, 100),
(7, 2, 2, 20),
(8, 3, 1, 1),
(9, 1, 3, 1),
(10, 2, 4, 10),
(11, 1, 4, 5),
(12, 5, 4, 33),
(13, 2, 5, 1),
(14, 2, 6, 8),
(15, 1, 6, 10),
(16, 1, 5, 20),
(17, 6, 5, 1),
(18, 7, 5, 1),
(19, 8, 5, 2),
(20, 9, 7, 3),
(21, 4, 8, 7),
(22, 3, 8, 8),
(23, 2, 8, 5),
(24, 1, 8, 7),
(25, 9, 9, 3),
(26, 8, 9, 4),
(27, 7, 9, 1),
(28, 6, 9, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `uf`
--

CREATE TABLE `uf` (
  `id` int(11) NOT NULL,
  `sigla` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `uf`
--

INSERT INTO `uf` (`id`, `sigla`) VALUES
(1, 'RS');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `email` varchar(120) NOT NULL,
  `senha` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `email`, `senha`) VALUES
(1, 'Roberto Pinho', 'robertoopinho@gmail.com', '808b54f0dc1082c417c17a016899c930');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `cidade`
--
ALTER TABLE `cidade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uf_id` (`uf_id`);

--
-- Índices para tabela `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `cidade_id` (`cidade_id`);

--
-- Índices para tabela `fornecedores`
--
ALTER TABLE `fornecedores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cidade_id` (`cidade_id`);

--
-- Índices para tabela `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Índices para tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `marca_id` (`marca_id`),
  ADD KEY `categoria_id` (`categoria_id`),
  ADD KEY `fornecedor_id` (`fornecedor_id`);

--
-- Índices para tabela `produto_pedido`
--
ALTER TABLE `produto_pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produto_id` (`produto_id`),
  ADD KEY `pedido_id` (`pedido_id`);

--
-- Índices para tabela `uf`
--
ALTER TABLE `uf`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `cidade`
--
ALTER TABLE `cidade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `fornecedores`
--
ALTER TABLE `fornecedores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `marcas`
--
ALTER TABLE `marcas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de tabela `produto_pedido`
--
ALTER TABLE `produto_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de tabela `uf`
--
ALTER TABLE `uf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `cidade`
--
ALTER TABLE `cidade`
  ADD CONSTRAINT `cidade_ibfk_1` FOREIGN KEY (`uf_id`) REFERENCES `uf` (`id`);

--
-- Limitadores para a tabela `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`cidade_id`) REFERENCES `cidade` (`id`);

--
-- Limitadores para a tabela `fornecedores`
--
ALTER TABLE `fornecedores`
  ADD CONSTRAINT `fornecedores_ibfk_1` FOREIGN KEY (`cidade_id`) REFERENCES `cidade` (`id`);

--
-- Limitadores para a tabela `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `pedido_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`);

--
-- Limitadores para a tabela `produtos`
--
ALTER TABLE `produtos`
  ADD CONSTRAINT `produtos_ibfk_1` FOREIGN KEY (`marca_id`) REFERENCES `marcas` (`id`),
  ADD CONSTRAINT `produtos_ibfk_2` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`),
  ADD CONSTRAINT `produtos_ibfk_3` FOREIGN KEY (`fornecedor_id`) REFERENCES `fornecedores` (`id`);

--
-- Limitadores para a tabela `produto_pedido`
--
ALTER TABLE `produto_pedido`
  ADD CONSTRAINT `produto_pedido_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`),
  ADD CONSTRAINT `produto_pedido_ibfk_2` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
